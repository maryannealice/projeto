package br.com.logique.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Associate.class)
public abstract class Associate_ {

	public static volatile SingularAttribute<Associate, String> city;
	public static volatile SingularAttribute<Associate, String> phone2;
	public static volatile SingularAttribute<Associate, String> type_associate;
	public static volatile SingularAttribute<Associate, String> zip_code;
	public static volatile SingularAttribute<Associate, String> phone1;
	public static volatile SingularAttribute<Associate, String> number;
	public static volatile SingularAttribute<Associate, String> rg;
	public static volatile SingularAttribute<Associate, String> street;
	public static volatile SingularAttribute<Associate, String> name;
	public static volatile SingularAttribute<Associate, String> cpf;
	public static volatile SingularAttribute<Associate, Integer> id_associate;
	public static volatile SingularAttribute<Associate, String> state;
	public static volatile SingularAttribute<Associate, Dependent> dependent;

}

