package br.com.logique.service;


import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import br.com.logique.dao.EmployeeDAO;
import br.com.logique.model.Employee;
import br.com.logique.model.Responsible;

@Path("/e")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class EmployeeService {
	
	EmployeeDAO dao = new EmployeeDAO();
	
	@Path("/insertEmp")
	@POST
	public void insertEmployee(Employee emp) {
		dao.insertEmployee(emp);
	}
	
	@Path("/validation")
	@POST
	public Employee validation(Employee emp) {
		return dao.validation(emp);
	}
	@Path("/listEmp")
	@GET
	public ArrayList<Employee> listEmployee() {
		return dao.listEmployee();
	}
	
}