package br.com.logique.config;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import br.com.logique.model.Associate;
import br.com.logique.model.Dependent;
import br.com.logique.model.Employee;
import br.com.logique.model.Responsible;

public class Hibernate {

	private static Configuration cfg;
	private static SessionFactory sessionFactory = buildSessionFactory();
		
	public Hibernate(){ }
	
	private synchronized static final SessionFactory buildSessionFactory() {
		try {
			if (sessionFactory == null) {
				cfg = new Configuration();

				cfg.setProperty("hibernate.dialect", "org.hibernate.dialect.PostgreSQLDialect");
	    		cfg.setProperty("hibernate.connection.driver_class", "org.postgresql.Driver");
				cfg.setProperty("hibernate.temp.use_jdbc_metadata_defaults", "false");
				cfg.setProperty("hibernate.current_session_context_class", "org.hibernate.context.internal.ThreadLocalSessionContext");

				cfg.setProperty("hibernate.c3p0.min_size", "0");
				cfg.setProperty("hibernate.c3p0.max_size", "1");
				cfg.setProperty("hibernate.c3p0.max_statements", "1");
				cfg.setProperty("hibernate.c3p0.maxIdleTime", "1");
				cfg.setProperty("hibernate.c3p0.acquireIncrement", "1");
				cfg.setProperty("hibernate.c3p0.initialPoolSize", "1");
				cfg.setProperty("hibernate.c3p0.maxIdleTimeExcessConnections", "1");
				
				cfg.setProperty("hibernate.show_sql", "true");
				cfg.setProperty("hibernate.format_sql", "true");
				cfg.setProperty("hibernate.hbm2ddl.auto", "create");
				cfg.setProperty("hibernate.connection.url", "jdbc:postgresql://127.0.0.1:5432/db_club");
				cfg.setProperty("hibernate.connection.username", "postgres");
				cfg.setProperty("hibernate.connection.password", "123456");

				cfg.addAnnotatedClass(Associate.class);
				cfg.addAnnotatedClass(Dependent.class);
				cfg.addAnnotatedClass(Employee.class);
				cfg.addAnnotatedClass(Responsible.class);
				
				return cfg.buildSessionFactory();

			} else {
				return sessionFactory;
			}
			
		} catch (Throwable ex) {
			System.out.println(ex);
			throw new ExceptionInInitializerError(ex);
		}
	}
	
	public synchronized static final Session getSession() {
		return sessionFactory.getCurrentSession();
	}
	
	public static void beginTransaction() {
		Hibernate.getSession().getTransaction().begin();
	}

	public static void commitTransaction() {
		Hibernate.getSession().getTransaction().commit();
	}

	public static void rollbackTransaction() {
		Hibernate.getSession().getTransaction().rollback();
	}

	
}