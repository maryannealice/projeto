package br.com.logique.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="dependent")
@SequenceGenerator(name = "id_dependent", sequenceName = "id_seq_dependent", initialValue = 1, allocationSize = 1)
public class Dependent {
	
	public int id_dependent, id_responsible;
	
	public Associate associate;
	
	 public Dependent() {  }
	
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_associate")
	public Associate getAssociate() {
		return associate;
	}

	public void setAssociate(Associate associate) {
		this.associate = associate;
	}
	
	public Responsible responsible;
	
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_responsible")
	public Responsible getResponsible() {
		return responsible;
	}

	public void setResponsible(Responsible responsible) {
		this.responsible = responsible;
	}


	@Id
	@Column(name = "id_dependent")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="id_dependent")
	public int getId_dependent() {
		return id_dependent;
	}
	
//	@Column(name="id_associate")
//	public int getId_associate() {
//		return id_associate;
//	}

	@Column(name="id_responsable")
	public int getId_responsible() {
		return id_responsible;
	}

	
//	public void setId_associate(int id_associate) {
//		this.id_associate = id_associate;
//	}

	public void setId_dependent(int id_dependent) {
		this.id_dependent = id_dependent;
	}

	public void setId_responsible(int id_responsible) {
		this.id_responsible = id_responsible;
	}
	
}