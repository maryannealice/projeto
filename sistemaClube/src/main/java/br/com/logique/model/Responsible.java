package br.com.logique.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="responsible")
@SequenceGenerator(name = "id_responsible", sequenceName = "id_seq_responsible", initialValue = 1, allocationSize = 1)
public class Responsible{
	
	public int id_responsible;	
	public String bank, agency, count;
	
	public Dependent dependent;
	
    public Associate associate;
    
	public Responsible() { }

	
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_associate", nullable=false, updatable = false, insertable = true)
    public Associate getAssociate() {
		return associate;
	}

	public void setAssociate(Associate associate) {
		this.associate = associate;
	}

	
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "responsible")
	public Dependent getDependent() {
		return dependent;
	}

	public void setDependent(Dependent dependent) {
		this.dependent = dependent;
	}
		

	@Id
	@Column(name = "id_responsible")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="id_responsible")
	public int getId_responsible() {
		return id_responsible;
	}
	
	@Column(name="bank")
	public String getBank() {
		return bank;
	}
	
	@Column(name="agency")
	public String getAgency() {
		return 	agency;
	}
	
	@Column(name="count")
	public String getCount() {
		return count;
	}	
	
	public void setId_responsible(int id_responsible) {
		this.id_responsible = id_responsible;
	}

	public void setBank(String bank) {
		this.bank = bank;
	}
	public void setAgency(String 	agency) {
		this.	agency = 	agency;
	}
	public void setCount(String count) {
		this.count = count;
	}
	
}