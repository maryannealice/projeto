package br.com.logique.utils;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;


public class CORSFilter implements Filter {

	public CORSFilter() {  }
	
	public void destroy() {  }

	 public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) 
			 throws IOException, ServletException {
		 ((HttpServletResponse) response).addHeader("Access-Control-Allow-Origin", "*");
		 ((HttpServletResponse) response).addHeader ("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
		 ((HttpServletResponse) response).addHeader ("Access-Control-Allow-Headers", 
				"Access-Control-Allow-Origin, Access-Control-Allow-Methods, Content-Type, Access-Control-Allow-Headers, "
				+ "Authorization, X-Requested-With, headers, app, token");
        chain.doFilter(request, response);
	}

	public void init(FilterConfig arg0) throws ServletException {  }
	
}