package sistemaClube;

import br.com.logique.dao.AssociateDAO;
import br.com.logique.dao.EmployeeDAO;
import br.com.logique.dao.ResponsibleDAO;
import br.com.logique.model.Associate;
import br.com.logique.model.Employee;
import br.com.logique.model.Responsible;

public class Test {

	public static void main(String[] args) {
		
		Employee e = new Employee();
		e.setLogin("func1");
		e.setPassword("123");
		EmployeeDAO d = new EmployeeDAO();
		d.insertEmployee(e);
		
		Associate a = new Associate();
		a.setType_associate("responsible");
		a.setName("João");
		a.setCpf("000.000.000-50");
		a.setRg("002.969.350");
		a.setPhone1("(94)8888-8888");
		a.setPhone2("(94)88888-8888");
		a.setZip_code("59.417-720");
		a.setStreet("rua ua");;
		a.setCity("Parnamirim");
		a.setState("RN");
		a.setNumber("204");
		
		AssociateDAO ad = new AssociateDAO();
		ad.insertAssociate(a);
		
		Responsible r = new Responsible();
		r.setAgency("5090");
		r.setBank("banco");
		r.setCount("0057961");
		r.setAssociate(a);
		
		ResponsibleDAO rd = new ResponsibleDAO();
		rd.insertResponsible(r);

	}
}
